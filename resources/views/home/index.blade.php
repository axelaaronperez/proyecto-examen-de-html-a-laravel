@extends('home.template')

@section('contenido')

@include('parcial.gestionremota')
<div class = "row">
        <div class = "col l2 s12">


        </div>
        <div class = "col l8 s12">
          <div class = "row">
            <div class = "col l6 s12">
              <div class="container-left">
                <h4 class="green-text">Funciones del Navegador</h4>
              <p class="green-text">Kiosk Browsere <font class="black-text ">tiene un amplio conjunto de funciones. Puede <br> probar estas funciones durante 5 días 
                simplemente <br> instalando, se requiere una licencia para uso personal y <br> comercial.</font></p>
              
              </div>
              <div class = "col l4 s12">

                <img src="img/google-play-badge.png" class = "responsive-img"alt="">


              </div>
  
            </div>
            <div class = "col l6 s12 center-align">
  
              <img src="img/candado.png" class = "responsive-img"alt="">
      
            </div>
  
          </div>
  
        </div>

      </div>
<div class = "row">
        <div class = "col l2 s12">


        </div>
        <div class = "col l8 s12">
          <div class = "row">
            <div class = "col l6 s12 center-align">
              
              <img src="img/candado2.png" class = "responsive-img"alt="">
  
            </div>
            <div class = "col l6 s12">
              <div class="container-left">
                <h4 class="orange-text">Funciones del Lanzador</h4>
              <p class="orange-text">Kiosk Browsere <font class="black-text ">tiene un amplio conjunto de funciones. <br>
                 Puede probar estas funciones durante 5 días simplemente <br> instalando, se requiere una licencia para uso personal y <br> comercial..</font></p>
              
              </div>
      
            </div>
  
          </div>
  
        </div>

      </div>
<div class="container">
        <h4 class="green-text text-darken-2">Opiniones de clientes</h4>
        <div class="row">
            <div class="col l6 m3 s12">
                <blockquote style="border-color: #2b7a0c">
                    <p align="justify">Cuando comenzamos a trabajar con esta aplicación, <br> tenía una curva de aprendizaje
                        bastante empinada. Estos <br> chicos han estado trabajando diligentemente en la <br>
                        construcción de su documentación, escuchando a sus <br> clientes y agregando
                        correcciones / características <br> adicionales; y ha valido la pena! Esta aplicación vale
                        cada <br> centavo si está buscando usarla comercialmente como lo <br> hemos hecho
                        nosotros.
                    </p>
                    <span class="orange-text text-darken-2">
                        Gunther Vinson, TowMate LLC
                    </span>
                </blockquote>
            </div>
            <div class="col l6 m3 s12">
                <blockquote style="border-color: #2b7a0c">
                    <p align="justify">Gran producto, soporte increíble . Usamos este producto <br> en aproximadamente 40
                        ubicaciones en nuestra empresa <br> y funciona perfectamente todo el día, todos los días.
                    </p>
                    <span class="orange-text text-darken-2">
                        David Higginson
                    </span>
                </blockquote >
                <blockquote style="border-color: #2b7a0c"> 
                    <p align="justify">Perfecto , utilícelo para nuestro quiosco de <br> reserva de canchas de tenis con una
                        aplicación web Google Apps <br> Script. Funciona perfectamente.
                    </p>
                    <span class="orange-text text-darken-2">
                        Serge Gravelle
                    </span>
                </blockquote>
            </div>
        </div>
    </div>

@endsection