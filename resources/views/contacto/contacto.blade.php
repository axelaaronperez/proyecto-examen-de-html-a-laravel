@extends('home.template')

@section('contenido')


    <div class = "row grey darken-3">
      <div class="col l2 s12">
        
      </div>
      <div class = "col l8 s12 center-align">
       <h5 class = "white-text">Ponerse en Contacto</h5>
       <p class = "white-text">¿Necesitas contactarnos? Inicie un chat o envíenos un correo electrónico.</p>
       <input class="btn white green-text" type="button" value="Habla con Nosotros"> 
       <br>
       <br>
       <br>


      </div>
      <div class = "col l2 s12">

      </div>
      
      </div>
      <div class = "row">
        <div class = "col l2 s12">


        </div>
        <div class = "col l8 s12">
          <div class = "card-panel center-align">
            <h4 class = "black-text">Envíanos un correo electrónico</h4> 
            <h6 class = "black-text">Su correo electrónico se enviará automáticamente a través de nuestro sistema de chat en vivo, se le enviará un correo electrónico cuando respondamos.</h6>
            <input class="black-text" type="text" value="Correo"> <br>
            <input class="black-text" type="text" value="Mensaje"> <br> <br>
            <button type="submit" class="btn left green darken-2 white-text text-darken-2"> Enviar <i class="material-icons">send</i></button> <br> <br>


          </div>
        </div>

      </div>
     

    
@endsection