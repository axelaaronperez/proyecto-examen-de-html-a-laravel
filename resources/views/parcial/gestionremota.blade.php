<div class="row grey darken-3">
    <div class="col l2"></div>
    <div class="col l4">
      <h4 class="white-text">Gestión remota de bloqueo</h4>
      <br>
      <p class="white-text">Administre todas sus instalaciones de <font class="green-text 
        text-darken-2">Kiosk Browser</font> / <font class="orange-text text-darken-4">Kiosk 
          Launcher</font> con nuestra consola
        de administración central.</p>
        <input class="btn green darken-2" type="button" value="Iniciar Sesión">
        <br>
        <br>
        <input class="btn orange darken-4" type="button" value="Registrarse">
    </div>
    <div class="col l4">
      <img src="img/escritorio.png" class="responsive-img" alt="">
    </div>
    <div class="col l2"></div>
</div>