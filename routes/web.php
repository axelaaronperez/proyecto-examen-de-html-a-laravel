<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as' => 'home', function () {
    return view('home.index');
}]);

Route::get('/caracteristicas', function () {
    return view('caracteristicas.caracteristicas');
})->name('caracteristicas');

//Route::get('/contacto', function () {
    //return view('contacto.contacto');
//})->name('contacto');

Route::get('/contacto','ControllerRutaContacto@RutaContacto')->name('contacto');
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
